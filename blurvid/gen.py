from PIL import ImageFilter, Image
import os
from random import randint
import shutil

# settings
imPath = "test.jpg"
outPath = "testFast.mp4"
tmpFolder = "out"
sacN = 1000
fpsac = 1
fps = 20
blinkFrame = False
blinkTarget = False

sacW = 400
dotW = 10

# emty out folder
try:
    shutil.rmtree(tmpFolder)
    print("deleted folder")
    os.remove(outPath)
    print("deleted outPath")
except Exception as e:
    print(e)

os.mkdir(tmpFolder)

# read base image
im = Image.open(imPath)
im.convert('RGBA')
w, h = im.size
if h > w:
    print("image needs to be landscape")
    quit()
if w < 1280:
    print("image needs to be at least 1280 wide")
    quit()

# create transparant mask
mask = Image.new('L', (3, 3), color=0xFF)
mask.putpixel((1,1), 0)
mask = mask.resize((sacW, sacW))
mask = mask.filter(ImageFilter.GaussianBlur(radius=sacW//8))
mask.save("mask.jpg")
print("made alpha mask")


# create blurred version
imBlur = im.filter(ImageFilter.GaussianBlur(radius=10)).convert('RGBA')

# make initial frame black for video preview frame
imWhite = Image.new('RGB', (w, h), (255, 255, 255))
imWhite.save(tmpFolder + "/" + str(0) + ".jpg")

# create frames
n = 1
x0, y0 = 0, 0
imCrop0 = None
for i in range(sacN):
    # cut a crop
    x, y = randint(0, w-sacW), randint(0, h-sacW)
    imCrop = im.crop((x, y, x+sacW, y+sacW)).convert('RGBA')
    imCropBlur = imBlur.crop((x, y, x+sacW, y+sacW)).convert('RGBA')
    imCropBlur.putalpha(mask)
    imCrop = Image.alpha_composite(imCrop, imCropBlur)
    if n == 1:
        imCrop.save("imCrop.png")
    # compose the image
    bg = Image.new('RGB', (w, h), (255, 255, 255))
    bg.paste(imBlur, (0,0))
    if imCrop0 is None:
        imCrop0 = imCrop.copy()
        x0, y0 = x, y
    bg.paste(imCrop0, (x0,y0)) # also draw the last saccade unblurred
    imCrop0 = imCrop.copy()
    bg.paste(imCrop, (x,y))
    # create a version with a white dot and black dot
    dot1 = Image.new('RGB', (dotW,dotW), (255, 255, 255))
    bg2 = bg.copy()
    dotOff = (sacW//2)-(dotW//2)
    bg.paste(dot1, (x+dotOff, y+dotOff))
    if blinkTarget:
        dot2 = Image.new('RGB', (dotW,dotW), (0, 0, 0))
        bg2.paste(dot2, (x+dotOff, y+dotOff))
    x0, y0 = x, y
   
    # flash all white, to hide saccade movement
    if blinkFrame:
        imWhite.save(tmpFolder + "/" + str(n) + ".jpg")
    n += 1
    # save saccades 
    for j in range(fpsac):
        if n%2 == 0 or not blinkTarget:
            bg.save(tmpFolder + "/" + str(n) + ".jpg")
        else: 
            bg2.save(tmpFolder + "/" + str(n) + ".jpg")
        n += 1

        if n%50 == 0:
            print("drawn", n, "frames")

print("made images, now making video")

# use ffmpeg to create a video from the frames
os.system("./makevid.sh "+tmpFolder+" "+outPath+" "+str(fps))
print("done!")
